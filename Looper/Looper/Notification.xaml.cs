﻿using LibLooper;
using LibLooper.LooperJobs.RestModels;
using LibLooper.Utils;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewLooper
{
    /// <summary>
    /// Interaction logic for Notification.xaml
    /// </summary>
    public partial class Notification : Window
    {
        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);
        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr hwnd, IntPtr dc);

        private LooperConfig config = new LooperConfig();
        private int warningLevel = 0;
        private string m_ticketId = "0";
        private static object m_lockerScreen = new object();
        private static object m_lockerWarning = new object();

        public delegate void CloseEventHandler(Notification notification);
        private event CloseEventHandler onClose;

        public event CloseEventHandler OnClose
        {
            add
            {
                onClose += value;
            }
            remove
            {
                onClose -= value;
            }
        }

        public string TicketId
        {
            get
            {
                return m_ticketId;
            }
            set
            {
                m_ticketId = value;
                ticketID.Text = value.ToString();
            }
        }

        public int Slot { get; set; } = 0;

        public string NotificationIndex { get; set; } = "";

        public System.Timers.Timer Timer { get; set; }

        public string TicketTitle
        {
            get
            {
                return ticketTitle.Text as string;
            }
            set
            {
                ticketTitle.Text = value;
            }
        }

        public string TicketSource { get; set; } = "0";

        public string TicketSummary
        {
            get
            {
                return ticketSummary.MarqueeContent;
            }
            set
            {
                ticketSummary.MarqueeContent = value;
            }
        }

        public int WarningLevel
        {
            get
            {
                return warningLevel;
            }
            set
            {
                warningLevel = value;
                switch(warningLevel)
                {
                    case 1:
                    case 2:
                        blinkScreen();
                        break;
                }
            }
        }

        public LooperData SourceItem { get; set; }

        public string Severity
        {
            get
            {
                return ticketSeverity.Text;
            }
            set
            {
                ticketSeverity.Text = value;
                switch(value)
                {
                    case "4":
                    case "3":
                        ticketSeverity.Background = (System.Windows.Media.Brush)new BrushConverter().ConvertFromString("#ffff00");
                        ticketSeverityLabel.Background = (System.Windows.Media.Brush)new BrushConverter().ConvertFromString("#ffff00");
                        break;
                    case "2":
                        ticketSeverity.Background = (System.Windows.Media.Brush)new BrushConverter().ConvertFromString("#ff0000");
                        ticketSeverityLabel.Background = (System.Windows.Media.Brush)new BrushConverter().ConvertFromString("#ff0000");
                        break;
                    case "1":
                    case "0":
                        mainButton.Background = (System.Windows.Media.Brush)new BrushConverter().ConvertFromString("#ff0000");
                        blinkScreen();
                        break;
                }
            }
        }

        public Notification(LooperConfig conf)
        {
            InitializeComponent();
            Loaded += Notification_Loaded;
            if(conf != null)
            {
                if(conf.FlashScreenOnNotification == true)
                {
                    blinkScreen();
                }
            }
        }

        private void Notification_Loaded(object sender, RoutedEventArgs e)
        {
            initMarquee();
            initWindow();
        }

        private Thread t = null;
        private void blinkScreen()
        {
            t = new Thread(() =>
            {
                try
                {
                    lock (m_lockerScreen)
                    {
                        int counter = 0;
                        while (true)
                        {
                            if (counter >= 5)
                            {
                                break;
                            }
                            Stopwatch s = new Stopwatch();
                            s.Start();
                            while (s.Elapsed < TimeSpan.FromSeconds(1))
                            {
                                IntPtr desktopPtr1 = GetDC(IntPtr.Zero);
                                Graphics g1 = Graphics.FromHdc(desktopPtr1);

                                SolidBrush b1 = new SolidBrush(System.Drawing.Color.Red);
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(0, 0, 10, 1080));
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(10, 1070, 1920, 1080));
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(1910, 0, 1910, 1080));
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(0, 0, 1920, 10));

                                g1.Dispose();
                                ReleaseDC(IntPtr.Zero, desktopPtr1);
                            }
                            s.Stop();
                            Stopwatch s2 = new Stopwatch();
                            s2.Start();
                            while (s2.Elapsed < TimeSpan.FromSeconds(1))
                            {
                                IntPtr desktopPtr1 = GetDC(IntPtr.Zero);
                                Graphics g1 = Graphics.FromHdc(desktopPtr1);

                                SolidBrush b1 = new SolidBrush(System.Drawing.Color.Yellow);
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(0, 0, 10, 1080));
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(10, 1070, 1920, 1080));
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(1910, 0, 1910, 1080));
                                g1.FillRectangle(b1, new System.Drawing.Rectangle(0, 0, 1920, 10));

                                g1.Dispose();
                                ReleaseDC(IntPtr.Zero, desktopPtr1);
                            }
                            s2.Stop();
                            counter++;
                        }
                        IntPtr desktopPtr = GetDC(IntPtr.Zero);
                        Graphics g = Graphics.FromHdc(desktopPtr);

                        SolidBrush b = new SolidBrush(System.Drawing.Color.Transparent);
                        g.FillRectangle(b, new System.Drawing.Rectangle(0, 0, 10, 1080));
                        g.FillRectangle(b, new System.Drawing.Rectangle(10, 1070, 1920, 1080));
                        g.FillRectangle(b, new System.Drawing.Rectangle(1910, 0, 1910, 1080));
                        g.FillRectangle(b, new System.Drawing.Rectangle(0, 0, 1920, 10));

                        g.Dispose();
                        ReleaseDC(IntPtr.Zero, desktopPtr);
                    }
                }
                catch
                {
                    return;
                }
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        private void initMarquee()
        {
            MouseEnter += new MouseEventHandler(delegate (object sender2, MouseEventArgs e2)
            {
                e2.Handled = true;
                ticketSummary.ResumeAnimation();
            });
            MouseLeave += new MouseEventHandler(delegate (object sender2, MouseEventArgs e2)
            {
                e2.Handled = true;
                ticketSummary.PauseAnimation();
            });
            ticketSummary.MarqueeTimeInSeconds = 6;
        }

        private void initWindow()
        {
            WindowInteropHelper wndHelper = new WindowInteropHelper(this);

            int exStyle = (int)GetWindowLong(wndHelper.Handle, (int)GetWindowLongFields.GWL_EXSTYLE);

            exStyle |= (int)ExtendedWindowStyles.WS_EX_TOOLWINDOW;
            SetWindowLong(wndHelper.Handle, (int)GetWindowLongFields.GWL_EXSTYLE, (IntPtr)exStyle);
        }

        private void showWarning()
        {
            lock(m_lockerWarning)
            {
                int counter = 0;
                while (true)
                {
                    if (counter >= 30)
                    {
                        break;
                    }
                    Stopwatch s = new Stopwatch();
                    s.Start();
                    while (s.Elapsed < TimeSpan.FromSeconds(1))
                    {
                        try
                        {
                            changeBackgroundColor("#DDFF9900");
                        }
                        catch (Exception ex)
                        {
                            Logger.logExceptionToFile("Notification.Notification_Loaded::timer_background_set==false", ex);
                        }
                    }
                    s.Stop();
                    Stopwatch s2 = new Stopwatch();
                    s2.Start();
                    while (s2.Elapsed < TimeSpan.FromSeconds(1))
                    {
                        try
                        {
                            changeBackgroundColor("#FF00AAFF");
                        }
                        catch (Exception ex)
                        {
                            Logger.logExceptionToFile("Notification.Notification_Loaded::timer_background_set==true", ex);
                        }
                    }
                    s2.Stop();
                    counter++;
                }
            }
        }

        private void changeBackgroundColor(string color)
        {
            Dispatcher.Invoke(() =>
            {
                var bc = new BrushConverter();
                mainButton.Background = (System.Windows.Media.Brush)bc.ConvertFromString(color);
            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(TicketId.ToString());
            onClose?.Invoke(this);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if(t != null)
            {
                t.Abort();
            }
            IntPtr desktopPtr = GetDC(IntPtr.Zero);
            Graphics g = Graphics.FromHdc(desktopPtr);

            SolidBrush b = new SolidBrush(System.Drawing.Color.Transparent);
            g.FillRectangle(b, new System.Drawing.Rectangle(0, 0, 10, 1080));
            g.FillRectangle(b, new System.Drawing.Rectangle(10, 1070, 1920, 1080));
            g.FillRectangle(b, new System.Drawing.Rectangle(1910, 0, 1910, 1080));
            g.FillRectangle(b, new System.Drawing.Rectangle(0, 0, 1920, 10));

            g.Dispose();
            ReleaseDC(IntPtr.Zero, desktopPtr);

            onClose?.Invoke(this);
        }

        #region Window styles

        [Flags]
        public enum ExtendedWindowStyles
        {
            // ...
            WS_EX_TOOLWINDOW = 0x00000080,
            // ...
        }

        public enum GetWindowLongFields
        {
            // ...
            GWL_EXSTYLE = (-20),
            // ...
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);

        public static IntPtr SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
        {
            int error = 0;
            IntPtr result = IntPtr.Zero;
            // Win32 SetWindowLong doesn't clear error on success
            SetLastError(0);

            if (IntPtr.Size == 4)
            {
                // use SetWindowLong
                Int32 tempResult = IntSetWindowLong(hWnd, nIndex, IntPtrToInt32(dwNewLong));
                error = Marshal.GetLastWin32Error();
                result = new IntPtr(tempResult);
            }
            else
            {
                // use SetWindowLongPtr
                result = IntSetWindowLongPtr(hWnd, nIndex, dwNewLong);
                error = Marshal.GetLastWin32Error();
            }

            if ((result == IntPtr.Zero) && (error != 0))
            {
                Logger.logExceptionToFile("Notification.SetWindowLong", new System.ComponentModel.Win32Exception(error));
                //throw new System.ComponentModel.Win32Exception(error);
            }

            return result;
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr", SetLastError = true)]
        private static extern IntPtr IntSetWindowLongPtr(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong", SetLastError = true)]
        private static extern Int32 IntSetWindowLong(IntPtr hWnd, int nIndex, Int32 dwNewLong);

        private static int IntPtrToInt32(IntPtr intPtr)
        {
            return unchecked((int)intPtr.ToInt64());
        }

        [DllImport("kernel32.dll", EntryPoint = "SetLastError")]
        public static extern void SetLastError(int dwErrorCode);

        #endregion
    }
}