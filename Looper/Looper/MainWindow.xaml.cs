﻿using LibLooper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Threading;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using LibLooper.Utils;
using LibLooper.LooperJobs;
using Utilities.Web.WebBrowserHelper;
using Looper;
using System.Net;

namespace NewLooper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<NotificationSlot> allSlots = new List<NotificationSlot>();
        private NotifyIcon trayIcon;
        private System.Windows.Forms.ContextMenuStrip trayMenu;
        private ToolStripMenuItem showWindowItem;

        private Dictionary<string, Notification> m_allNotifications = new Dictionary<string, Notification>();

        private LooperBacklogManager looperManager = new LooperBacklogManager();
        private LooperBacklogManager entryChecker = new LooperBacklogManager();
        private Tracker tracker = new Tracker();
        private QueryParser parser = null;
        private SyncConfig config = null;
        private bool playSound = false;
        private static int slotCount = 12;
        private List<string> m_ignoredNotifications = new List<string>();
        private SoundManager soundManager = new SoundManager();

        public MainWindow() {
            InitializeComponent();
            looperManager.TickTime = Looper.Properties.Settings.Default.TickTime;
            looperManager.NewTicket += looperManagerCollectionChanged;
            entryChecker.TickTime = Looper.Properties.Settings.Default.TickTimeEntryChecker;
            entryChecker.NewTicket += entryCheckerCollectionChanged;

            looperManager.InitLooperBacklogManager();
            entryChecker.InitLooperBacklogManager();
            /*
             * This function starts all registered loopers/queries for TFS or IcM
             */
            looperManager.StartAll();
            entryChecker.StartAll();

            /*WebRequest request = WebRequest.CreateHttp("http://vstfrd:8080/Azure/_apis/projects?api-version=2.0");
            request.UseDefaultCredentials = true;
            WebResponse response = request.GetResponse();
            StreamReader stream = new StreamReader(response.GetResponseStream());
            System.Windows.MessageBox.Show(stream.ReadToEnd());*/

            initGui();
        }

        private void entryCheckerCollectionChanged(LooperData newTicket) {
            if (newTicket != null && DashboardItemsSource.AllItems.ContainsKey(newTicket.Id)) {
                if (DashboardItemsSource.AllItems[newTicket.Id].State.ToString() != newTicket.State) {
                    DashboardItemsSource.AllItems[newTicket.Id].State = newTicket.State;
                }
            }
        }

        private void looperManagerCollectionChanged(LooperData newTicket) {
            createNewNotification(newTicket);
            if (!DashboardItemsSource.AllItems.ContainsKey(newTicket.Id)) {
                try {
                    Dispatcher.Invoke(() => {
                        DashboardItemsSource.AllItems.Add(newTicket.Id, new DashboardEntry() {
                            SrcId = newTicket.Id,
                            Source = newTicket.Source,
                            State = newTicket.State,
                            Title = newTicket.Title,
                            Type = newTicket.Type
                        });
                    });
                } catch (Exception ex) {
                    Logger.logExceptionToFile("MainWindow.WiBacklog_CollectionChanged", ex);
                }
            }
        }

        private void createNewNotification(LooperData sourceItem) {
            for (int i = 0; i < m_allNotifications.Count; i++) {
                if (m_allNotifications.Values.ToArray()[i].TicketId == sourceItem.Id && m_allNotifications.Values.ToArray()[i].TicketSource == sourceItem.Source) {
                    if (sourceItem.State.ToLowerInvariant() == "resolved") {
                        m_allNotifications.Values.ToArray()[i].Close();
                        m_allNotifications.Remove(m_allNotifications.Values.ToArray()[i].NotificationIndex);
                    } else {
                        m_allNotifications[m_allNotifications.Values.ToArray()[i].NotificationIndex].TicketTitle = sourceItem.Config.Name;
                        m_allNotifications[m_allNotifications.Values.ToArray()[i].NotificationIndex].TicketSummary = sourceItem.Title;
                        m_allNotifications[m_allNotifications.Values.ToArray()[i].NotificationIndex].WarningLevel = sourceItem.WarningLevel;
                        m_allNotifications[m_allNotifications.Values.ToArray()[i].NotificationIndex].Severity = sourceItem.Severity;
                        m_allNotifications[m_allNotifications.Values.ToArray()[i].NotificationIndex].SourceItem = sourceItem;
                    }
                    return;
                }
            }
            NotificationSlot currentSlot = null;
            foreach (NotificationSlot slot in allSlots) {
                if (!slot.InUse) {
                    slot.InUse = true;
                    currentSlot = slot;
                    break;
                }
            }
            if (currentSlot == null) {
                currentSlot = allSlots[1];
            }
            string rndName = LibLooper.Utils.Random.RandomName(16);
            try {
                Dispatcher.Invoke(() => {
                    m_allNotifications.Add(rndName, new Notification(sourceItem.Config));
                    m_allNotifications[rndName].WindowStartupLocation = WindowStartupLocation.Manual;
                    m_allNotifications[rndName].Left = SystemParameters.WorkArea.Width - 340;
                    m_allNotifications[rndName].TicketId = sourceItem.Id;
                    m_allNotifications[rndName].TicketTitle = sourceItem.Config.Name;
                    m_allNotifications[rndName].OnClose += notification_OnClose;
                    m_allNotifications[rndName].NotificationIndex = rndName;
                    m_allNotifications[rndName].TicketSource = sourceItem.Config.Source;
                    m_allNotifications[rndName].TicketSummary = sourceItem.Title;
                    m_allNotifications[rndName].WarningLevel = sourceItem.WarningLevel;
                    m_allNotifications[rndName].Severity = sourceItem.Severity;
                    m_allNotifications[rndName].SourceItem = sourceItem;
                    double height = SystemParameters.WorkArea.Height;
                    double hMarg = (height - (slotCount * m_allNotifications[rndName].Height)) / slotCount;
                    m_allNotifications[rndName].Top = currentSlot.Top;
                    m_allNotifications[rndName].Show();
                });
            } catch (Exception ex) {
                Logger.logExceptionToFile("MainWindow.CreateNewNotification", ex);
            }
            if (playSound == true && sourceItem.Config.SoundFile.Length > 0) {
                soundManager.PlaySoundFile(sourceItem.Config.SoundFile);
            }
        }

        private void initGui() {
            Uri iconUri = new Uri("list.png", UriKind.Relative);
            try {
                Icon = BitmapFrame.Create(iconUri);
            } catch (Exception ex) {
                Logger.logExceptionToFile(this, ex);
            }
            trayMenu = new System.Windows.Forms.ContextMenuStrip();
            Closing += mainWindow_Closing;  // Bind the MainWindow_Closed function to the closed event from the MainWindow

            parser = new QueryParser();  // Initialises the config-parser
            config = SyncConfig.LoadConfig("config.json");

            playSound = config.PlaySound;  // Load from the config, if the sound should be played as a default
            dashboardGrid.ItemsSource = DashboardItemsSource.AllItems;
            dashboardGrid.Loaded += dashboardGrid_Loaded;
            buildNotificationSlots();
            initTrayIcon();
            Hide();  // initially hides the dashboard window
        }

        #region trayMenuMethods

        private void initTrayIcon() {
            // Button to send a feedback to the developer
            ToolStripItem mailItem = new ToolStripButton {
                Text = "SendFeedback"
            };
            mailItem.Click += sendFeedback;
            trayMenu.Items.Add(mailItem);

            //Checkbox if a sound should be played when somthing comes in
            ToolStripMenuItem playSoundCheckbox = new ToolStripMenuItem {
                CheckOnClick = true,
                Text = "PlaySound"
            };
            playSoundCheckbox.CheckedChanged += playSoundCheckbox_CheckedChanged;
            playSoundCheckbox.Checked = playSound;
            trayMenu.Items.Add(playSoundCheckbox);

            //Checkbox for the tracker for fairfax powerbi
            if (Looper.Properties.Settings.Default.TrackerFunctionAvailable == true) {
                ToolStripMenuItem registerUserCheckbox = new ToolStripMenuItem {
                    CheckOnClick = true,
                    Text = "User available",
                    Checked = playSound
                };
                registerUserCheckbox.CheckedChanged += registerUserCheckbox_CheckedChanged;
                trayMenu.Items.Add(registerUserCheckbox);
            }

            LibLooper.LooperMain.loadLooperConfig(config, parser, looperManager, trayMenu, looperCheckbox_CheckedChanged);//Load all loopers/queries from the config and add a checkbox to the tray-menu

            //Button to deactivate all activated loopers
            ToolStripItem allLooperDeactivateItem = new ToolStripButton {
                Text = "Deactivate all loopers"
            };
            allLooperDeactivateItem.Click += allLooperDeactivateItem_Click;
            trayMenu.Items.Add(allLooperDeactivateItem);

            //Button to show the dashboard with an overview of all fetched tasks after the start of the application
            showWindowItem = new ToolStripMenuItem {
                CheckOnClick = true,
                Text = "Show Dashboard"
            };
            showWindowItem.CheckedChanged += showWindowItem_Click;
            trayMenu.Items.Add(showWindowItem);

            ToolStripItem clearCacheItem = new ToolStripButton {
                Text = "Clear Cache"
            };
            clearCacheItem.Click += clearCacheItem_Click;
            trayMenu.Items.Add(clearCacheItem);

            //Button to quit the application
            ToolStripItem exitItem = new ToolStripButton {
                Text = "Quit"
            };
            exitItem.Click += exitEvent;
            trayMenu.Items.Add(exitItem);

            //Initilise the icon in the notification area in the taskbar
            trayIcon = new NotifyIcon {
                Text = "Looper",
                Icon = new Icon(SystemIcons.Application, 40, 40),
                ContextMenuStrip = trayMenu,
                Visible = true
            };
        }

        private void clearCacheItem_Click(object sender, EventArgs e) {
            WebBrowserHelper.ClearCache();

            closeAllNotifications();
            looperManager.CancelAll();
            tracker.Stop();
            try { Close(); } catch { }
            App.Current.Shutdown(0);
            try { Environment.Exit(0); } catch { }
        }

        private void exitEvent(object sender, EventArgs e)
        {
            closeAllNotifications();
            looperManager.CancelAll();
            tracker.Stop();
            try { Close(); } catch { }
            App.Current.Shutdown(0);
            try { Environment.Exit(0); } catch { }
        }

        private void showWindowItem_Click(object sender, EventArgs e) {
            if (sender.GetType() == typeof(ToolStripMenuItem)) {
                ToolStripMenuItem mItem = sender as ToolStripMenuItem;
                if (mItem.Checked) {
                    Show();
                } else {
                    Hide();
                }
            }
        }

        private void registerUserCheckbox_CheckedChanged(object sender, EventArgs e) {
            if (sender.GetType() == typeof(ToolStripMenuItem)) {
                ToolStripMenuItem mItem = sender as ToolStripMenuItem;
                if (mItem.Checked) {
                    tracker.Start();
                } else {
                    tracker.Pause();
                }
            }
        }

        private void playSoundCheckbox_CheckedChanged(object sender, EventArgs e) {
            if (sender.GetType() == typeof(ToolStripMenuItem)) {
                ToolStripMenuItem item = sender as ToolStripMenuItem;
                looperManager.PlaySound = item.Checked ? true : false;
            }
        }

        /*
         * This function is raised, if on one of the loopers registered in the config file is clicked in the menu
         */
        private void looperCheckbox_CheckedChanged(object sender, EventArgs e) {
            if (sender.GetType() == typeof(ToolStripMenuItem)) {
                ToolStripMenuItem looperItem = (ToolStripMenuItem)sender;
                string looper_name = looperItem.Name;
                if (looperItem.Checked) {
                    looperManager.ResumeJob(looper_name);
                } else {
                    looperManager.PauseJob(looper_name);
                }
            }
        }

        private void allLooperDeactivateItem_Click(object sender, EventArgs e) {
            looperManager.PauseAll();
        }

        private static void sendFeedback(object sender, EventArgs e) {
            Thread newThread = new Thread(() => {
                Outlook.Application oApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                oMailItem.To = "v-luldke@microsoft.com";
                oMailItem.Subject = "[Looper][Feedback]";
                oMailItem.Display(false);
            });
            newThread.Start();
        }

        private void buildNotificationSlots() {
            double topScreen = SystemParameters.WorkArea.Top;
            double bottomScreen = SystemParameters.WorkArea.Bottom;
            double heightScreen = bottomScreen - topScreen;
            int slotCount = (int)heightScreen / (50 + 10);
            for (int i = 0; i < slotCount; i++) {
                allSlots.Add(new NotificationSlot() { InUse = false, Top = (heightScreen - ((50 + 10) * i)), SlotIndex = i });
            }
            allSlots[0].InUse = true;
        }

        private void mainWindow_Closing(object sender, CancelEventArgs e) {
            e.Cancel = true;
            showWindowItem.Checked = false;
        }

        private void dashboardGrid_Loaded(object sender, RoutedEventArgs e) {
            int height = (int)dashboardGrid.ActualHeight / 2;
            dashboardGrid.RowHeight = 30;
        }

        private void closeAllNotifications() {
            foreach (KeyValuePair<string, Notification> info in m_allNotifications) {
                try {
                    Dispatcher.Invoke(() => {
                        info.Value.Close();
                    });
                } catch (Exception ex) {
                    Logger.logExceptionToFile("MainWindow.closeAllNotifications", ex);
                }
            }
        }

        #endregion

        #region Dashboard

        #region DashboardSearch
        private void startSearch(object sender, RoutedEventArgs e) {
            DashboardItemsSource.SearchResult.Clear();
            for (int i = 0; i < DashboardItemsSource.AllItems.Count; i++) {
                if (DashboardItemsSource.AllItems.Values.ToArray()[i].Title.ToLowerInvariant().Contains(searchBox.Text.ToLowerInvariant()) || DashboardItemsSource.AllItems.Values.ToArray()[i].Type.ToLowerInvariant() == searchBox.Text.ToLowerInvariant() || DashboardItemsSource.AllItems.Values.ToArray()[i].State.ToString().ToLowerInvariant().Contains(searchBox.Text.ToLowerInvariant()) || DashboardItemsSource.AllItems.Values.ToArray()[i].SrcId.ToLowerInvariant().Contains(searchBox.Text.ToLowerInvariant())) {
                    DashboardItemsSource.SearchResult.Add(DashboardItemsSource.AllItems.ToArray()[i]);
                }
            }
            dashboardGrid.ItemsSource = DashboardItemsSource.SearchResult;
        }

        private void resetSearch(object sender, RoutedEventArgs e) {
            dashboardGrid.ItemsSource = DashboardItemsSource.AllItems;
        }

        #endregion

        /*
         * when the user double-clicks on a row-entry the id will be copied to clipboard
         */
        private void dashboard_MouseDoubleClick(object sender, RoutedEventArgs e) {
            if (e.Source.GetType() == typeof(DataGridRow)) {
                DataGridRow dgRow = e.Source as DataGridRow;
                if (dgRow.Item.GetType() == typeof(KeyValuePair<string, DashboardEntry>)) {
                    KeyValuePair<string, DashboardEntry> item = (KeyValuePair<string, DashboardEntry>)dgRow.Item;
                    System.Windows.Clipboard.SetText(item.Value.SrcId);
                }
            }
        }

        #endregion

        #region Notification
        /*
         * executed when inside a notification the onClose event is fired
         */
        private void notification_OnClose(Notification notification) {
            for (int i = 1; i < allSlots.Count; i++) {
                allSlots[i].InUse = false;
            }
            m_allNotifications.Remove(notification.NotificationIndex);
            try {
                Dispatcher.Invoke(() => {
                    notification.OnClose -= notification_OnClose;
                    if (notification.Timer != null) {
                        notification.Timer.Stop();
                        notification.Timer.Close();
                    }
                    notification.Close();
                });
            } catch (Exception ex) {
                Logger.logExceptionToFile("MainWindow.Notification_OnClose", ex);
            }
            allSlots[0].InUse = true;
            for (int i = 0; i < m_allNotifications.Count; i++) {
                for (int x = 1; x < allSlots.Count; x++) {
                    if (allSlots[x].InUse == false) {
                        m_allNotifications[m_allNotifications.ToArray()[i].Value.NotificationIndex].Slot = allSlots[x].SlotIndex;
                        m_allNotifications[m_allNotifications.ToArray()[i].Value.NotificationIndex].Top = allSlots[x].Top;
                        allSlots[x].InUse = true;
                        break;
                    }
                }
            }
        }

        #endregion
    }
}
