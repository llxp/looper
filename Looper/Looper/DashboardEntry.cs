﻿namespace NewLooper
{
    internal class DashboardEntry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public object State { get; set; }
        public string Source { get; set; }
        public string SrcId { get; set; }
    }
}