﻿using System.Collections.ObjectModel;

namespace NewLooper
{
    static class DashboardItemsSource
    {
        public static ObservableDictionary<string, DashboardEntry> AllItems = new ObservableDictionary<string, DashboardEntry>();
        public static ObservableDictionary<string, DashboardEntry> SearchResult = new ObservableDictionary<string, DashboardEntry>();
    }
}
