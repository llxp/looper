﻿namespace NewLooper
{
    internal class NotificationSlot
    {
        public bool InUse = false;
        public double Top = 0;
        public int SlotIndex = 0;
    }
}