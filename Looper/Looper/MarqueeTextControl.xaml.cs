﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewLooper
{
    /// <summary>
    /// Interaction logic for MarqueeTextControl.xaml
    /// </summary>
    public partial class MarqueeTextControl : UserControl
    {
        MarqueeType _marqueeType;

        public static readonly DependencyProperty MarqueeContentProperty = DependencyProperty.Register("MarqueeContent", typeof(string), typeof(MarqueeTextControl), new PropertyMetadata());

        private AnimationClock clock;
        public void PauseAnimation()
        {
            if(clock != null)
            {
                clock.Controller.Pause();
            }
        }

        public void ResumeAnimation()
        {
            if(clock != null)
            {
                clock.Controller.Resume();
            }
        }

        public MarqueeType MarqueeType
        {
            get { return _marqueeType; }
            set { _marqueeType = value; }
        }

        public String MarqueeContent
        {
            set { tbmarquee.Text = value; }
            get
            {
                return tbmarquee.Text;
            }
        }

        private double _marqueeTimeInSeconds;

        public double MarqueeTimeInSeconds
        {
            get { return _marqueeTimeInSeconds; }
            set { _marqueeTimeInSeconds = value; }
        }


        public MarqueeTextControl()
        {
            InitializeComponent();
            canMain.Height = this.Height;
            canMain.Width = this.Width;
            this.Loaded += new RoutedEventHandler(MarqueeText_Loaded);
        }

        void MarqueeText_Loaded(object sender, RoutedEventArgs e)
        {
            StartMarqueeing(_marqueeType);
        }



        public void StartMarqueeing(MarqueeType marqueeType)
        {
            if (marqueeType == MarqueeType.LeftToRight)
            {
                LeftToRightMarquee();
            }
            else if (marqueeType == MarqueeType.RightToLeft)
            {
                RightToLeftMarquee();
            }
            else if (marqueeType == MarqueeType.TopToBottom)
            {
                TopToBottomMarquee();
            }
            else if (marqueeType == MarqueeType.BottomToTop)
            {
                BottomToTopMarquee();
            }
        }

        private void LeftToRightMarquee()
        {
            if(canMain.ActualWidth <= tbmarquee.ActualWidth)
            {
                double height = canMain.ActualHeight - tbmarquee.ActualHeight;
                tbmarquee.Margin = new Thickness(0, height / 2, 0, 0);
                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = canMain.ActualWidth - tbmarquee.ActualWidth;
                doubleAnimation.To = 0;
                doubleAnimation.RepeatBehavior = new RepeatBehavior(1);
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(_marqueeTimeInSeconds));

                DoubleAnimation doubleAnimation2 = new DoubleAnimation();
                doubleAnimation2.From = 0;
                doubleAnimation2.To = canMain.ActualWidth - tbmarquee.ActualWidth;
                doubleAnimation2.RepeatBehavior = new RepeatBehavior(1);
                doubleAnimation2.Duration = new Duration(TimeSpan.FromSeconds(_marqueeTimeInSeconds));

                doubleAnimation2.Completed += new EventHandler(delegate (object sender2, System.EventArgs e2)
                {
                    //tbmarquee.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                    //doubleAnimation.BeginTime = TimeSpan.FromSeconds(_marqueeTimeInSeconds);
                    clock.Controller.Stop();
                    clock = doubleAnimation.CreateClock();
                    tbmarquee.ApplyAnimationClock(Canvas.LeftProperty, clock);
                });

                doubleAnimation.Completed += new EventHandler(delegate (object sender, System.EventArgs e)
                {
                    //tbmarquee.BeginAnimation(Canvas.LeftProperty, doubleAnimation2);
                    clock.Controller.Stop();
                    clock = doubleAnimation2.CreateClock();
                    tbmarquee.ApplyAnimationClock(Canvas.LeftProperty, clock);
                });
                clock = doubleAnimation.CreateClock();
                tbmarquee.ApplyAnimationClock(Canvas.LeftProperty, clock);
                PauseAnimation();
                //tbmarquee.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            }
            else
            {
                tbmarquee.Width = canMain.ActualWidth;
            }
        }
        private void RightToLeftMarquee()
        {
            double height = canMain.ActualHeight - tbmarquee.ActualHeight;
            tbmarquee.Margin = new Thickness(0, height / 2, 0, 0);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.From = -tbmarquee.ActualWidth;
            doubleAnimation.To = canMain.ActualWidth;
            doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(_marqueeTimeInSeconds));
            tbmarquee.BeginAnimation(Canvas.RightProperty, doubleAnimation);
        }
        private void TopToBottomMarquee()
        {
            double width = canMain.ActualWidth - tbmarquee.ActualWidth;
            tbmarquee.Margin = new Thickness(width / 2, 0, 0, 0);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.From = -tbmarquee.ActualHeight;
            doubleAnimation.To = canMain.ActualHeight;
            doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(_marqueeTimeInSeconds));
            tbmarquee.BeginAnimation(Canvas.TopProperty, doubleAnimation);
        }
        private void BottomToTopMarquee()
        {
            double width = canMain.ActualWidth - tbmarquee.ActualWidth;
            tbmarquee.Margin = new Thickness(width / 2, 0, 0, 0);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.From = -tbmarquee.ActualHeight;
            doubleAnimation.To = canMain.ActualHeight;
            doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(_marqueeTimeInSeconds));
            tbmarquee.BeginAnimation(Canvas.BottomProperty, doubleAnimation);
        }
    }
    public enum MarqueeType
    {
        LeftToRight,
        RightToLeft,
        TopToBottom,
        BottomToTop
    }
}
